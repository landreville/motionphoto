```
usage: motionphoto.py [-h] [-d DESTINATION] [-D] PHOTOS_PATH

Extract video from motion photo.

positional arguments:
  PHOTOS_PATH           Path to photos to extract video from.

optional arguments:
  -h, --help            show this help message and exit
  -d DESTINATION, --destination DESTINATION
                        Directory to place extracted video files. Default is
                        the same as photo.
  -D, --delete          Delete original photo file after extraction.
```
