import argparse
import glob
import logging
import os.path
import os
from libxmp.utils import file_to_dict
from libxmp import XMPFiles, consts

log = logging.getLogger(__name__)


GCAMERA_NS = 'http://ns.google.com/photos/1.0/camera/'


def main(file_paths, destination, delete, keep):
    for file_path in file_paths:
        extract_video(file_path, destination, delete, keep)


def extract_video(file_path, destination, delete, keep):
    gcamera_xmp = parse_xmp_to_dict(
        file_to_dict(file_path)[GCAMERA_NS]
    )
    if not has_microvideo(gcamera_xmp):
        log.info(f'No Micro Video found. {file_path}')
        return

    offset = os.stat(file_path).st_size - int(gcamera_xmp['GCamera:MicroVideoOffset'])

    if offset <= 0:
        log.info(f'Skipping due to negative video offset. {file_path}')
        return

    with open(file_path, 'rb') as photo_file:
        photo_data = photo_file.read(offset)
        photo_file.seek(offset)
        video_data = photo_file.read()

    video_file_name = os.path.basename(file_path).rsplit('.')[0] + '.mp4'
    if destination:
        video_file_path = destination
    else:
        video_file_path = os.path.join(
            os.path.dirname(file_path), video_file_name
        )
    with open(video_file_path, 'wb') as video_file:
        video_file.write(video_data)

    if delete:
        os.remove(file_path)
    elif not keep:
        with open(file_path, 'wb') as photo_file:
            # TODO: remove GCamera microvideo metadata
            photo_file.write(photo_data)


def parse_xmp_to_dict(xmp):
    return {k: v for k, v, md in xmp}


def has_microvideo(xmp):
    return 'GCamera:MicroVideo' in xmp and xmp['GCamera:MicroVideo'] == '1'


if __name__ == '__main__':
    logging.basicConfig(level=logging.DEBUG)
    parser = argparse.ArgumentParser(
        description='Extract video from motion photo.'
    )
    parser.add_argument(
        'photos_path',
        metavar='PHOTOS_PATH',
        help='Path to photos to extract video from.'
    )
    parser.add_argument(
        '-d',
        '--destination',
        metavar='DESTINATION',
        default=None,
        help=
        'Directory to place extracted video files. Default is the same as photo.'
    )
    parser.add_argument(
        '-D',
        '--delete',
        action='store_true',
        default=False,
        help='Delete original photo file after extraction.'
    )
    parser.add_argument(
        '-k', '--keep',
        action='store_true',
        default=False,
        help='Keep the video embedded within the JPEG. '
             'Default is to remove the video data from the photo.'
    )

    args = parser.parse_args()
    files = glob.iglob(args.photos_path)
    main(files, args.destination, args.delete, args.keep)
